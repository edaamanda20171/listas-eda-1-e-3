package br.ufc.eda.q1

trait IHashTable {

def criar(n: Int): HashTable;
def inserir(hash: HashTable, valor: Integer):Unit;
def buscar(hash: HashTable, valor : Integer): Integer;
def remover(hash: HashTable, valor: Integer): HashTable;
def liberar(hash: HashTable);
}