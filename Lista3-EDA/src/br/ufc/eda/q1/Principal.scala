package br.ufc.eda.q1

object Principal {
	def main(args: Array[String]){

	  var obj: Object = new HashTable();
	  var ht: HashTable = null;
	  ht = (obj.asInstanceOf[HashTable]).criar(20);
	  println("Tamanho da hash  " + ht.vetor.length);
	  
	  ht.inserir(ht, 2);
	  ht.inserir(ht, 13);
	  ht.inserir(ht, 1);
	  ht.inserir(ht, 3);
	  ht.inserir(ht, 4);
	  
	  println(ht.buscar(ht, 2));
	  println(ht.buscar(ht, 13));
	  println(ht.buscar(ht, 1));
	  println(ht.buscar(ht, 4));
	  
	  ht.remover(ht, 1);
	  
	  println(ht.buscar(ht, 2));
	  println(ht.buscar(ht, 13));
	  println(ht.buscar(ht, 1));
	  println(ht.buscar(ht, 4));
	  
	  ht.liberar(ht)
	 
	  
	}
}