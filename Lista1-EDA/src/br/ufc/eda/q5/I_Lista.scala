package br.ufc.eda.q5

trait I_Lista {
  def criar():Lista;
  def inserir(l: Lista, valor: Integer):Lista;
  def imprimir(l: Lista):Unit;
  def imprimirRec(l: Lista):Unit;
  def vazia(l:Lista):Int;
  def buscar(l:Lista, valor:Integer):Lista;
  def remover(l:Lista, valor: Integer):Lista;
  def removerRec(l:Lista, valor: Integer):Lista;
  def liberar(l:Lista):Unit;
 
}