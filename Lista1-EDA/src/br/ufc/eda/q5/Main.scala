package br.ufc.eda.q5

object Main {
	def main(args: Array[String]){
	  
	  //questão 5.1
	  var obj: Object = new Lista();
	  var lst: Lista = null;
	  lst = (obj.asInstanceOf[Lista]).criar();
	  
	  
	  //questão 5.2
	 
	  lst = lst.inserir(lst, 1)
	  lst = lst.inserir(lst, 3)
	  lst = lst.inserir(lst, 7)
	  lst = lst.inserir(lst, 2)
	  lst = lst.inserir(lst, 3)
	  lst = lst.inserir(lst, 6)


	  //questão 5.3
	  lst.imprimir(lst);  
	  println("");
	  
	  //questão 5.4
	  lst.imprimirRec(lst);
	  
	  println("");
	  //questão 5.5
	  var empty = lst.vazia(lst);
	  println(empty)
	  
	  //questão 5.6
	  println("")
	  var listaBusca = lst.buscar(lst, 1);
	  println("Resultado da busca: " + listaBusca.info);
	  
	  //questão 5.7  
	  println("")
	  lst = lst.remover(lst, 7);
	  lst.imprimir(lst);
	  
	 //questão 5.8  
	  println("")
	  lst = lst.removerRec(lst, 3);
	  lst.imprimir(lst); 
	  	  
	  //questão 5.9
	  println("")
	  lst.liberar(lst)
  

	  
	  
	}

}