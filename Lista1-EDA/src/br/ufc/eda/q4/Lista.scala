package br.ufc.eda.q4

//SIMPLESMENTE ENCADEADA CIRCULAR
class Lista extends I_Lista{

	var proximo : Lista = _;
  var info : Integer = _;
  var topo : Lista = _;
  var fim: Lista = _;
  

override def criar() : Lista = {
		var lst = new Lista();
		return lst
}


override def inserir(l: Lista, valor: Integer): Lista = {
  if(valor == null){
    println("Impossível imprimir, elemento nulo")
    return l
  }
  
  if(vazia(l) == 1){
    var novaLista = criar();
    novaLista.topo = novaLista;
    novaLista.fim = novaLista;
    novaLista.info = valor;
    novaLista.proximo = null;
    return novaLista;
  }
  
    var lista = l
    var novaLista = criar();
    
    novaLista.topo = novaLista;
    novaLista.fim = lista.fim;
    novaLista.info = valor;
    novaLista.proximo = lista;
    novaLista.fim.proximo = novaLista.topo;
  return novaLista;
}

override def imprimir(l:Lista):Unit = {
		if(vazia(l) == 1){
			println("Lista Vazia")
		}else{
			var lista = l;
			var count = 0
			while(count < 1){
				print(lista.info + " ");
				lista = lista.proximo;
				if(lista == lista.fim.proximo){
				  count +=1
				}
			}			
		}

}

override def imprimirRec(l: Lista):Unit = {
		    var lista = l
				if(vazia(l) == 0){
				  print(lista.info + " ")
					if(lista != lista.fim) imprimirRec(lista.proximo)
				}
}


override def vazia(l:Lista):Int ={
		if(l.info == null && l.proximo == null) return 1
				else return 0
}

override def buscar(l:Lista, valor:Integer):Lista = {
		var lista = l

		  if(l.info == valor){
		    return l
		  }
		    lista = lista.proximo;
				while(lista != lista.fim.proximo){
					if(lista.info == valor){
						return lista;
					}
					lista = lista.proximo;

				}
		println("O elemento não está na lista")
		return l;
}

override def remover(l:Lista, valor: Integer):Lista = {
		if(l.info == valor){
			var novaLista = l.proximo;
			novaLista.fim = l.fim;
			novaLista.fim.proximo = novaLista;
			novaLista.topo = novaLista;
			l.proximo = null;
			l.info = null;
			l.topo = null;
			l.fim = null;
			return novaLista;
		}         

		var lista = l;
		while (lista.proximo != lista.fim){
			if(lista.proximo.info == valor){
				lista.proximo = lista.proximo.proximo;
				return l
			}
				lista = lista.proximo   
		}
		
		if(lista.proximo.info == valor){
		  lista.proximo = lista.topo;
		  lista.fim = lista;
		}
		
		return l;
}


override def removerRec(l:Lista, valor: Integer):Lista = {
		if(vazia(l) == 0){
			if(l.info == valor){
				return l.proximo
			}else{
			  if(l.proximo != l.fim.proximo){
			   l.proximo = removerRec(l.proximo, valor) 
			  }
			} 
		}
		return l
}

override def liberar(l:Lista):Unit = {
		if(l.vazia(l) == 1){
		  println("A lista já está vazia")
		}else{
		  var aux = l
		  var remv = aux
		  while(aux != null){
		    aux.info = null
		    aux.proximo = null
		    remv = aux
		    aux = aux.proximo
		    remv = null
		  }
		}
}
}
