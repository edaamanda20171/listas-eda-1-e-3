package br.ufc.eda.q4

object Main {
	def main(args: Array[String]){
	  
	  //questão 4.1
	  var obj: Object = new Lista();
	  var lst: Lista = null;
	  lst = (obj.asInstanceOf[Lista]).criar();
	  
	  
	  //questão 4.2
	 
	  lst = lst.inserir(lst, 1)
	  lst = lst.inserir(lst, 3)
	  lst = lst.inserir(lst, 7)
	  lst = lst.inserir(lst, 2)
	  lst = lst.inserir(lst, 3)
	  lst = lst.inserir(lst, 6)


	  //questão 4.3
	  lst.imprimir(lst);  
	  println("");
	  
	  //questão 4.4
	  lst.imprimirRec(lst);
	  
	  println("");
	  //questão 4.5
	  var empty = lst.vazia(lst);
	  println(empty)
	  
	  //questão 4.6
	  println("")
	  var listaBusca = lst.buscar(lst, 2);
	  println("Resultado da busca: " + listaBusca.info);
	  
	  //questão 4.7  
	  println("")
	  lst = lst.remover(lst, 2);
	  lst.imprimir(lst);
	  
	 //questão 4.8  
	  println("")
	  lst = lst.removerRec(lst, 1);
	  lst.imprimir(lst);
	  
	  //questão 4.9
	  println("")
	  lst.liberar(lst)
  
	}

}