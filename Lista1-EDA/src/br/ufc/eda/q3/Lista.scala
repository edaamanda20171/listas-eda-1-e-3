package br.ufc.eda.q3
//DUPLAMENTE ENCADEADA ORDENADA
class Lista extends I_Lista{

	var proximo : Lista = _;
	var anterior : Lista = _;
  var info : Integer = _;

override def criar() : Lista = {
		var lst = new Lista();
		return lst
}

override def inserirOrd(l: Lista, valor: Integer):Lista = {
		if(valor == null){
			println("Impossível inserir, elemento nulo")
			return l
		}

		if(l.info == null){
			var novaLista: Lista = new Lista();
  		novaLista.info = valor;
  		novaLista.proximo = null;
  		novaLista.anterior = null;
  		return novaLista;
		}
		
		if(valor <= l.info){
			var novaLista: Lista = new Lista();
  		novaLista.info = valor;
  		novaLista.proximo = l;
  		l.anterior = novaLista;
  		novaLista.anterior = null;
  		return novaLista;
		}
		
		var lista = l;
		while(lista.proximo != null){
		  if(lista.proximo.info > valor){
		    var temp = lista.proximo;
		    var novaLista: Lista = new Lista();
		    novaLista.info = valor;
		    novaLista.anterior = lista;
		    lista.proximo = novaLista;
		    novaLista.proximo = temp;
		    novaLista.proximo.anterior = novaLista;	
		    return l
		  }
		    lista = lista.proximo;
		}
		
		if(lista.info <= valor){
		  var novaLista = new Lista();
		  novaLista.info = valor;
		  lista.proximo = novaLista;
		  novaLista.proximo = null;
		  novaLista.anterior = lista;		  
		}

		return l
}

override def imprimir(l:Lista):Unit = {
    var l2 : Lista = null;
		if(vazia(l) == 1){
			println("Lista Vazia")
		}else{
			    var lista = l
					while(lista != null){					  
						print(lista.info + " ")
						lista = lista.proximo
					}
					
		}
}

override def imprimirRec(l: Lista):Unit = {
		var lista = l
		if(lista != null){
			print(lista.info + " ")
			imprimirRec(lista.proximo)

		}
}

override def imprimirRev(l: Lista):Unit = {
  		var aux = l;
  		var temp = new Lista();
  		while(aux != null){
  		  aux = aux.proximo;
  		  if(aux != null){
  		    temp = aux;
  		  }
  		}
  		println("")
  		while(temp != null){
  		  print(temp.info + " ")
  		  temp = temp.anterior;
  		}
  
}

override def vazia(l:Lista):Int ={
		if(l.info == null && l.proximo == null && l.anterior == null) return 1
		else return 0
}

override def buscar(l:Lista, valor:Integer):Lista = {
		var lista = l

		while(lista != null){
			if(lista.info == valor){
				return lista;
			}
			lista = lista.proximo;

		}
println("O elemento não está na lista")
return l;
}

override def remover(l:Lista, valor: Integer):Lista = {
		if(l.info == valor){
			var newList = l.proximo
			l.proximo = null
			l.info = null
			return newList
		}         

		var lista = l;
		while (lista.proximo != null){
			if(lista.proximo.info == valor){
				lista.proximo = lista.proximo.proximo;
				return l
			}
				lista = lista.proximo  
			 
		}
		return l;
}


override def removerRec(l:Lista, valor: Integer):Lista = {
		if(vazia(l) == 0){
			if(l.info == valor){
			  var novaLista = l.proximo
			  novaLista.anterior = null;
				return novaLista;
			}else{
			  if(l.proximo != null){
			    l.proximo = removerRec(l.proximo, valor)  
			  }
				 

			} 
		}
		return l
}

override def liberar(l:Lista):Unit = {
		if(l.vazia(l) == 1){
			println("A lista já está vazia")
		}else{
			var aux = l
			var remv = aux
			while(aux != null){
				aux.info = null
				aux.proximo = null
				aux.anterior = null
				remv = aux
				aux = aux.proximo
				remv = null
			}
		}
}

def iguais(l1: Lista, l2: Lista): Int = {
  var tam1: Int = 0;
  var tam2: Int = 0;
  
  var listaAux = l1
  while(listaAux != null){
    tam1 += 1
    listaAux = listaAux.proximo
  }
  
  listaAux = l2
  while(listaAux != null){
    tam2 += 1
    listaAux = listaAux.proximo
  }
   
  if(tam1 != tam2){
    return 0
  }
  
  var lst1 = l1
  var lst2 = l2
  while(lst1 != null){
    if(lst1.info != lst2.info){
      return 0
    }
    lst1 = lst1.proximo
    lst2 = lst2.proximo
  }
  
  return 1
}

}
