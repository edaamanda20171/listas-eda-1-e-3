package br.ufc.eda.q3

object Main {
	def main(args: Array[String]){
	  
	  //questão 3.1
	  var obj: Object = new Lista();
	  var lst: Lista = null;
	  lst = (obj.asInstanceOf[Lista]).criar();
	  
	  //questão 3.2
	  println("Inserindo ordenadamente")
	  lst = lst.inserirOrd(lst, 2)
	  lst = lst.inserirOrd(lst, 1)
	  lst = lst.inserirOrd(lst, 3)
	  lst = lst.inserirOrd(lst, 7)
	  lst = lst.inserirOrd(lst, 5)

	  
	  //questão 3.3
	  println("Imprimindo iterativamente")
	  lst.imprimir(lst);
	  
	  println("");
	  //questão 3.4
	  println("Imprimindo recursivamente");
	  lst.imprimirRec(lst);
	  
	  println("");
	  
	  println("Imprimindo reversamente");
	  //questão 3.5
	  lst.imprimirRev(lst);
    
	  println("");
	  
	  //questão 3.6 --- retorna 1 se for vazia, do contrário 0
	  println("Verificando lista vazia");
	  var empty = lst.vazia(lst);
	  println(empty)
	  
	  println("");
	  
	  
	  
	  //questão 3.7
	  println("Realizando busca de um elemento");
	  var listaBusca = lst.buscar(lst, 10);
	  println("Busca: " + listaBusca.info);
	  
	  //questão 3.8  
	  println("Removendo iterativamente")
	  lst = lst.remover(lst, 7);
	  lst.imprimir(lst);
	  println("");
	 
	  //questão 3.9  
	  println("Removendo recursivamente")
	  lst = lst.removerRec(lst, 1);
	  lst.imprimir(lst);
	  println("");
	  
	  //questão 3.10
	  println("Liberando lista")
	  lst.liberar(lst)
	  
	  //questão 3.11 --- retorna 1 se forem iguais, do contrário 0
	  
	  println("Verificando se listas são iguais")
	  
	  var lst2 = new Lista();
	  lst2 = lst2.inserirOrd(lst2, 7)
	  lst2 = lst2.inserirOrd(lst2, 6)

	  
	  lst.imprimir(lst2)
	  println("")
	  
	  var lst1 = new Lista();
	  lst1 = lst.inserirOrd(lst1, 7)
	  lst1 = lst.inserirOrd(lst1, 6)
	
	  
	  lst.imprimir(lst1)
	  println("")
	  
	  var iguais = lst.iguais(lst1, lst2);
	  println(iguais)
	  
 	  

	  
	}

}