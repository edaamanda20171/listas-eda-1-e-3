package br.ufc.eda.q3

trait I_Lista {
  def criar():Lista;
  def inserirOrd(l: Lista, valor: Integer): Lista;
  def imprimir(l: Lista):Unit;
  def imprimirRec(l: Lista):Unit;
  def imprimirRev(l: Lista):Unit;
  def vazia(l:Lista):Int;
  def buscar(l:Lista, valor:Integer):Lista;
  def remover(l:Lista, valor: Integer):Lista;
  def removerRec(l:Lista, valor: Integer):Lista;
  def liberar(l:Lista):Unit;
  def iguais(l1: Lista, l2: Lista): Int;
 
}