package br.ufc.eda.q1
import br.ufc.eda.q1

object Main {
	def main(args: Array[String]){
	  
	  //questão 1.1
	  var obj: Object = new Lista();
	  var lst: Lista = null;
	  lst = (obj.asInstanceOf[Lista]).criar();
	  
	  
	  //questão 1.2
	  lst = lst.inserir(lst, 1);
	  lst = lst.inserir(lst, 2);
	  lst = lst.inserir(lst, 3);
	  lst = lst.inserir(lst, 4);
	  lst = lst.inserir(lst, 5);
	  lst = lst.inserir(lst, 6);
	  
	  //questão 1.3
	  lst.imprimir(lst);
	  
	  println("");
	  //questão 1.4
	  lst.imprimirRec(lst);
	  
	  println("");
	  //questão 1.5
	  lst.imprimirRev(lst);

	  println("");
	  //questão 1.6
	  var empty = lst.vazia(lst);
	  println(empty)
	  
	  println("");
	  //questão 1.7
	  var listaBusca = lst.buscar(lst, 1);
	  println("Resultado da busca: " + listaBusca.info);
	  
	  //questão 1.8  
	  println("")
	  lst = lst.remover(lst, 2);
	  lst.imprimir(lst);
	  
	   //questão 1.9  
	  println("")
	  lst = lst.removerRec(lst, 3);
	  lst.imprimir(lst);
	  
	  //questão 1.10
	  println("")
	  lst.liberar(lst)  
	  
	}

}