package br.ufc.eda.q1

//SIMPLESMENTE ENCADEADA
class Lista extends I_Lista{

	var proximo : Lista = _;
  var info : Integer = _;

override def criar() : Lista = {
		var lst = new Lista();
		return lst
}


override def inserir(l: Lista, valor: Integer): Lista = {
		if(valor == null){
			println("Impossível inserir, elemento nulo")
			return l
		}else if(l.info == null){
			var newList: Lista = new Lista();
		newList.info = valor;
		newList.proximo = null;
		return newList 
		}

		else{
			var newList: Lista = new Lista();
		newList.info = valor;
		newList.proximo = l;
		return newList 
		}
}

override def imprimir(l:Lista):Unit = {
		if(vazia(l) == 1){
			println("Lista Vazia")
		}else{
			var lista = l
			while(lista != null){
				print(lista.info + " ")
				lista = lista.proximo
			}
		}

}

override def imprimirRec(l: Lista):Unit = {
		var lista = l
				if(lista != null){
					print(lista.info + " ")
					imprimirRec(lista.proximo)

				}
}

override def imprimirRev(l: Lista):Unit = {
		var lista = l
				if(lista != null){
					imprimirRev(lista.proximo)
					print(lista.info + " ")

				}
}

override def vazia(l:Lista):Int ={
		if(l.info == null && l.proximo == null) return 1
				else return 0
}

override def buscar(l:Lista, valor:Integer):Lista = {
		var lista = l

				while(lista != null){
					if(lista.info == valor){
						return lista;
					}
					lista = lista.proximo;

				}
		println("O elemento não está na lista")
		return l;
}

override def remover(l:Lista, valor: Integer):Lista = {
		if(l.info == valor){
			var newList = l.proximo
					l.proximo = null
					l.info = null
					return newList
		}         

		var lista = l;
		while (lista.proximo != null){
			if(lista.proximo.info == valor){
				lista.proximo = lista.proximo.proximo;
				return l
			}
				lista = lista.proximo   
		}
		return l;
}


override def removerRec(l:Lista, valor: Integer):Lista = {
		if(vazia(l) == 0){
			if(l.info == valor){
				return l.proximo
			}else{
			  if(l.proximo != null){
			    l.proximo = removerRec(l.proximo, valor)   
			  }
				

			} 
		}
		return l
}

override def liberar(l:Lista):Unit = {
		if(l.vazia(l) == 1){
		  println("A lista já está vazia")
		}else{
		  var aux = l
		  var remv = aux
		  while(aux != null){
		    aux.info = null
		    aux.proximo = null
		    remv = aux
		    aux = aux.proximo
		    remv = null
		  }
		}
}

}
