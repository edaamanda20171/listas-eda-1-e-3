package br.ufc.eda.q2

object Main {
	def main(args: Array[String]){
	  
	  //questão 2.1
	  var obj: Object = new Lista();
	  var lst: Lista = null;
	  lst = (obj.asInstanceOf[Lista]).criar();
	  
	  //questão 2.2
	  println("Inserindo ordenadamente")
	  lst = lst.inserirOrd(lst, 7)
	  lst = lst.inserirOrd(lst, 6)
	  lst = lst.inserirOrd(lst, 5)
	  lst = lst.inserirOrd(lst, 4)
	  lst = lst.inserirOrd(lst, 7)
	  lst = lst.inserirOrd(lst, 3)
	  lst = lst.inserirOrd(lst, 0)
	  lst = lst.inserirOrd(lst, 1)
	  
	  //questão 2.3
	  println("Imprimindo iterativamente")
	  lst.imprimir(lst);
	  
	  println("");
	  //questão 2.4
	  println("Imprimindo recursivamente");
	  lst.imprimirRec(lst);
	  
	  println("");
	  
	  println("Imprimindo reversamente");
	  //questão 2.5
	  lst.imprimirRev(lst);
    
	  println("");
	  
	  //questão 2.6 --- retorna 1 se for vazia, do contrário 0
	  println("Verificando lista vazia");
	  var empty = lst.vazia(lst);
	  println(empty)
	  
	  println("");
	  println("Realizando busca de um elemento");
	  //questão 2.7
	  var listaBusca = lst.buscar(lst, 15);
	  println("Busca: " + listaBusca.info);
	  
	  //questão 2.8  
	  println("Removendo iterativamente")
	  lst = lst.remover(lst, 7);
	  lst.imprimir(lst);
	  println("");
	   //questão 2.9  
	  println("Removendo recursivamente")
	  lst = lst.removerRec(lst, 1);
	  lst.imprimir(lst);
	  println("");
	  //questão 2.10
	  println("Liberando lista")
	  lst.liberar(lst)
	  println("");
	  //questão 2.11 --- retorna 1 se forem iguais, do contrário 0
	  println("Verificando se listas são iguais")
	  
	  var lst2 = new Lista();
	  lst2 = lst2.inserirOrd(lst2, 7)
	  lst2 = lst2.inserirOrd(lst2, 6)
	  lst2 = lst2.inserirOrd(lst2, 5)
	  lst2 = lst2.inserirOrd(lst2, 4)
	  lst2 = lst2.inserirOrd(lst2, 7)
	  lst2 = lst2.inserirOrd(lst2, 3)
	  lst2 = lst2.inserirOrd(lst2, 0)
	  lst2 = lst2.inserirOrd(lst2, 1)
	  
	  lst.imprimir(lst2)
	  println("")
	  
	  var lst1 = new Lista();
	  lst1 = lst.inserirOrd(lst1, 7)
	  lst1 = lst.inserirOrd(lst1, 6)
	  lst1 = lst.inserirOrd(lst1, 5)
	  lst1 = lst.inserirOrd(lst1, 4)
	  lst1=  lst.inserirOrd(lst1, 7)
	  lst1 = lst.inserirOrd(lst1, 3)
	  lst1 = lst.inserirOrd(lst1, 0)
	  lst1 = lst.inserirOrd(lst1, 1)
	  
	  lst.imprimir(lst1)
	  println("")
	  
	  var iguais = lst.iguais(lst1, lst2);
	  println(iguais)
	  
 	  

	  
	}

}